import sys
import copy
import Posting
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.uic import loadUi
from PyQt5.QtCore import *

class daftarWidget(QWidget):
	updateList_signal = pyqtSignal()
	edit_signal = pyqtSignal(dict)
	def __init__(self,data):
		super(daftarWidget,self).__init__()
		loadUi('daftar.ui',self)
		self.data = data
		self.idLabel.setText(data['id'])
		self.judulLabel.setText(data['judul'])
		self.kontenBrowser.setText(data['konten'])
		self.waktuLabel.setText(data['waktu'])
		self.hapusBtn.clicked.connect(self.hapus)
		self.editBtn.clicked.connect(self.edit)
	def hapus(self):
		msg = QMessageBox()
		msg.setIcon(QMessageBox.Critical)
		msg.setText("Apakah Anda yakin untuk menghapus posting ini?")
		#msg.setInformativeText("This is additional information")
		msg.setWindowTitle("Konfirmasi Penghapusan")
		#msg.setDetailedText("The details are as follows:")
		msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
		retval = msg.exec_()
		if retval == QMessageBox.Ok :
			Posting.hapus(self.data['id'])
			self.updateList_signal.emit()
		
			
		
		
	def edit(self):
		self.edit_signal.emit(self.data)
		print('dari list')
class mainWidget(QWidget):
	edit_signal = pyqtSignal(dict)
	def __init__(self):
		super(mainWidget,self).__init__()
		loadUi('main.ui',self)
		self.page = 0
		self.updateList()
		self.nextBtn.clicked.connect(self.nextPage)
		self.prevBtn.clicked.connect(self.prevPage)
		self.updateState()
	def updateState(self):
		self.pageLabel.setText('Page : %d' % self.page)
		if self.page is 0:
			self.prevBtn.hide()
		if self.page > 0:
			self.prevBtn.show()
		if Posting.daftar(self.page + 1)['error'] is True:
			self.nextBtn.hide()
		else:
			self.nextBtn.show()
	def nextPage(self):
		self.page += 1
		self.updateList()
		self.updateState()
	def prevPage(self):
		if self.page is 0:
			self.page = 0
		else:
			self.page -= 1
		self.updateList()
		self.updateState()
	def updateList(self):
		self.listWidget.clear()
		daftar = Posting.daftar(self.page)
		
		for i in daftar['data']:
			itemN = QListWidgetItem()
			item = daftarWidget(i)
			item.updateList_signal.connect(self.updateList)
			item.edit_signal.connect(self.edit)
			self.listWidget.insertItem(self.listWidget.count(),itemN)
			self.listWidget.setItemWidget(itemN,item)
			itemN.setSizeHint(item.sizeHint())
	def edit(self,data):
		self.edit_signal.emit(data)
		print('dari main')
class editorWidget(QWidget):
	backToMain_signal = pyqtSignal()
	def __init__(self,data=None):
		super(editorWidget,self).__init__()
		loadUi('editor.ui',self)
		self.mode = 'tambah'
		if data is not None:
			self.data = data
			self.judulEdit.setText(data['judul'])
			self.kategoriEdit.setText(data['kategori'])
			self.kontenEdit.setPlainText(data['konten'])
			self.mode = 'edit'
		self.cancelBtn.clicked.connect(self.backToMain)
		self.postBtn.clicked.connect(self.send)
	def backToMain(self):
		self.backToMain_signal.emit()
	def send(self):
		data = {
				'judul' 	: self.judulEdit.text(),
				'kategori'	: self.kategoriEdit.text(),
				'konten'	: self.kontenEdit.toPlainText()
			}
		if self.mode is 'tambah':
			response = Posting.tambah(data)
		else:
			response = Posting.edit(self.data['id'],data)
		if response['error'] is False:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Information)
			msg.setText("Posting berjudul %s berhasil diedit" % data['judul'])
			#msg.setInformativeText("This is additional information")
			msg.setWindowTitle("Berhasil Edit Posting")
			#msg.setDetailedText("The details are as follows:")
			msg.setStandardButtons(QMessageBox.Ok)
			retval = msg.exec_()
			print(retval)
			self.backToMain()
		else:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Terdapat error dalam memasukkan data ke server")
			#msg.setInformativeText("This is additional information")
			msg.setWindowTitle("Error")
			msg.setDetailedText(response['data'])
			msg.setStandardButtons(QMessageBox.Ok)
			retval = msg.exec_()
			#print(retval)
class MainWindow(QMainWindow):
	
	def __init__(self):
		super().__init__()
		loadUi('init.ui',self)
		self.main = mainWidget()
		self.setCentralWidget(self.main)
		self.main.pushButton.clicked.connect(self.tambah)
		self.main.edit_signal.connect(self.edit)
		self.show()
	
	def tambah(self):
		self.editor = editorWidget()
		self.editor.backToMain_signal.connect(self.backToMain)
		self.setCentralWidget(self.editor)
	
	def edit(self,data):
		self.editor = editorWidget(data)
		self.editor.backToMain_signal.connect(self.backToMain)
		self.setCentralWidget(self.editor)
		print('oke')
	def backToMain(self):
		self.main = mainWidget()
		self.setCentralWidget(self.main)
		self.main.pushButton.clicked.connect(self.tambah)
		self.main.edit_signal.connect(self.edit)
app = QApplication(sys.argv)
window = MainWindow()
window.show()
sys.exit(app.exec_())
