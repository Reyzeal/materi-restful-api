import json
import requests

headers = {'Content-Type': 'application/json'}

def daftar(page = 0):
	url = 'http://127.0.0.11/post/halaman/%d' % page
	response = requests.get(url,headers=headers)
	if response.status_code is 200:
		return json.loads(response.content.decode('utf-8'))
	else:
		return None
		
def lihat(post_id):
	url = 'http://127.0.0.11/post/%d' % post_id
	response = requests.get(url,headers=headers)
	if response.status_code is 200:
		return json.loads(response.content.decode('utf-8'))
	else:
		return None
		
def tambah(post_data):
	url = 'http://127.0.0.11/post/tambah'
	response = requests.post(url,data = post_data)
	if response.status_code is 200:
		return json.loads(response.content.decode('utf-8'))
	else:
		return None

def edit(post_id,post_data):
	url = 'http://127.0.0.11/post/edit/%s' % post_id
	response = requests.post(url,data = post_data)
	if response.status_code is 200:
		return json.loads(response.content.decode('utf-8'))
	else:
		return None
		
def hapus(post_id):
	url = 'http://127.0.0.11/post/hapus/%s' % post_id
	response = requests.delete(url, headers=headers)
	if response.status_code is 200:
		return json.loads(response.content.decode('utf-8'))
	else:
		return None

#print(tambah({'judul':'oke','konten':"Hi! Using PyQt5, I'm trying to get a custom dialog (containing a simple QListWidget) to return a value. After much tweaking and looking online, I still can't find what's wrong with this code.The custom dialog is in this class:"}))
