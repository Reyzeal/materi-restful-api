# Aplikasi Blog Sederhana

## Melihat daftar posting
Alamat : `http://127.0.0.200/post`
## Melihat suatu Posting
Alamat : `http://127.0.0.200/post/{id}`
## Menambah Posting
Alamat : `http://127.0.0.200/post/tambah`
## Mengedit Posting
Alamat : `http://127.0.0.200/post/edit/{id}`
## Menghapus Posting
Alamat : `http://127.0.0.200/post/hapus/{id}`
