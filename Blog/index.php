<?php
// membersihkan url seperti /post/tambah?xxx menjadi array ['post','tambah']
$request_uri = $_SERVER['REQUEST_URI']; // raw request
$request_uri = substr($request_uri, 1);	// substring untuk menghilangkan '/' diawal
$request_uri = explode('/', $request_uri); // memisahkan url berdasar '/'

// membersihkan dari parameter seperti ?blablabla=bla&xxx=1
$request_uri[count($request_uri)-1] = explode('?', $request_uri[count($request_uri)-1])[0];

// include library
require_once('library/database.php');
require_once('library/input.php');
require_once('library/response_json.php');

// inisialisasi input baik melalui $_GET maupun $_POST
Input::set($_GET,$_POST);

/********ROUTING DISINI*****************/
// route 'post' untuk urusan posting
if($request_uri[0] == 'post'){
	switch ($request_uri[1]) {
		// url : post/tambah
		case 'tambah':
			require('controller/post/tambah.php');	
			break;
		// url : post/edit/{id}
		case 'edit':
			if(!isset($request_uri[2]) || $request_uri[2] == '')
				Response::failed('tidak menyertakan ID');
			$id = $request_uri[2];
			require('controller/post/edit.php');
			break;
		// url : post/hapus/{id}
		case 'hapus':
			if(!isset($request_uri[2]) || $request_uri[2] == '')
				Response::failed('tidak menyertakan ID');
			$id = $request_uri[2];
			require('controller/post/hapus.php');
			break;
		// url : post/halaman/{n}
		case 'halaman':
			if(!isset($request_uri[2]) || $request_uri[2] == '')
				$halaman = 0;
			else $halaman = $request_uri[2];

			$ukuran = 5;
			$awal = $halaman * $ukuran;
			require('controller/post/list.php');	
			break;
		default:
			// url : post/{id}
			$id = $request_uri[1];
			require('controller/post/lihat.php');
			break;
	}
}
