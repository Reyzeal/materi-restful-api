<?php
/**
	Author 	: Rizal Ardhi Rahmadani
	Date 	: 27 November 2018
	
	Kegunaan class Input adalah menghandle semua request baik
	berupa $_GET maupun $_POST dalam satu class
	fungsi yang tersedia hanya 2:

	set untuk menginisialisasi class
	gunakan : set($_GET,$_POST)
	
	get untuk mengambil data request
*/
class Input{
	private static $data;
	public static function set($get,$post){
		foreach ($get as $key => $value) {
			self::$data[$key] = $value;
		}
		foreach ($post as $key => $value) {
			self::$data[$key] = $value;
		}
	}
	public static function get($name){
		return self::$data[$name];
	}
}