<?php

/**
	Author 	: Rizal Ardhi Rahmadani
	Date 	: 27 November 2018
	
	Kegunaan class Response untuk membuat php melakukan encoding terhadap
	array/object yang diberikan menjadi json

*/

class Response{
	public static function success($data = ''){
		$json 		= [
			'error' 	=> false,
			'message'	=> 'success',
			'data'		=> $data
		];
		$encoded = json_encode($json);

		header('Content-type: application/json');
		echo $encoded;
		exit();
	}
	public static function failed($data = ''){
		$json 		= [
			'error' 	=> true,
			'message'	=> 'failed',
			'data'		=> $data
		];
		$encoded = json_encode($json);

		header('Content-type: application/json');
		echo $encoded;
		exit();
	}
}