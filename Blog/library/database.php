<?php
/**
	Author 	: Rizal Ardhi Rahmadani
	Date 	: 27 November 2018
	
	Kegunaan class DB adalah untuk menghubungkan php dengan mysql

	select -> untuk mengambil data dari database dan mengemasnya dalam bentuk object

	insert -> untuk mengeksekusi perintah sql tanpa perlu suatu return berupa data
*/
class DB{
	private static $hostname = 'localhost';
	private static $username = 'latihan';
	private static $password = 'justdoit';
	private static $database = 'latihan';
	private static function connect(){
		$connection = new Mysqli(self::$hostname,self::$username,self::$password,self::$database);
		if ($connection->connect_error) {
    		die('Connect Error (' . $mysqli->connect_errno . ') '.$mysqli->connect_error);
		}
		return $connection;
	}
	public static function select($table,$sql){
		$connection = self::connect();
		
		$result = $connection->query($sql);
		if ($result->num_rows > 1) {
			$data = [];
    		while ($row = $result->fetch_assoc()) {
    			$data[] = $row;
    		}
    		return $data;
		}
		else if ($result->num_rows == 1) {
			$data = $result->fetch_assoc();
			return $data;
		}
		return false;
	}
	public static function execute($table,$sql){
		$connection = self::connect();
		if ($connection->query($sql) === true) {
    		return true;
		}
		return false;
	}
}